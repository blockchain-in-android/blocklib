package net.anandu.blocklib.blockchain;

import android.content.Context;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.consensus.ConsensusInterface;
import net.anandu.blocklib.consensus.GenesisConfig;
import net.anandu.blocklib.database.Block;
import net.anandu.blocklib.database.BlockWithTransactions;
import net.anandu.blocklib.database.BlockchainDatabase;
import net.anandu.blocklib.database.Transaction;
import net.anandu.blocklib.database.TransactionStatus;
import net.anandu.blocklib.util.AuthorityCallback;
import net.anandu.blocklib.util.BlockCallback;
import net.anandu.blocklib.util.BlockListCallback;
import net.anandu.blocklib.util.CallbackInterface;
import net.anandu.blocklib.util.StringUtil;
import net.anandu.blocklib.util.TransactionListCallback;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public abstract class BlockChain implements BlockChainInterface {
    ConsensusInterface consensusInterface;
    BlockLib.TransactionValidator transactionValidator;
    BlockchainDatabase appDb;
    KeyPair keys;
    private Integer maxNumOfTransactions;

    public BlockChain(Context c, BlockLib.TransactionValidator transactionValidator) {
        this.transactionValidator = transactionValidator;
        appDb = BlockchainDatabase.getDatabase(c);
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            try {
                Block genesisBlock = appDb.blockDao().getBlockHeader(1);
                if (genesisBlock != null)
                    maxNumOfTransactions = Integer.parseInt(Objects.requireNonNull(genesisBlock.getData().get("Max Transactions Count")));
            } catch (Exception e) {
                //TODO: handle possible exceptions
                System.out.println(e);
            }
        });
    }

    @Override
    public void setConsensusMethod(ConsensusInterface c) {
        this.consensusInterface = c;
        this.consensusInterface.syncChain();
    }

    //TODO: Take key pair from the user in init function and store it
    @Override
    public void init(GenesisConfig config, KeyPair keys, Context context) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        //checking if blockchain exists
        this.keys = keys;
        Block genesisBlock = config.createBlock(keys);
        //start timer -- if timer expires -- check if we received any blocks by reading from db?
        //if no, execute below code
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
            //check if we received any blocks by reading from db
                BlockchainDatabase.databaseWriteExecutor.execute(() -> {
                    final long blocksCount;
                    blocksCount = appDb.blockDao().getBlocksCount();
                    if (blocksCount == 0) {
                        System.out.println("-------CREATING GENESIS BLOCK--------");
                        config.init(context);
                        System.out.println("MY PEER ID: " + genesisBlock.getValidator());
                        appDb.blockDao().insertBlock(genesisBlock, Collections.EMPTY_LIST);
                        maxNumOfTransactions = config.getMaxTransactionsCount();
                    } else {
//                throw new RuntimeException("Blockchain already exists");
                        System.out.println("!!!!!!!!GENESIS BLOCK ALREADY EXISTS!!!!!!!!!!");
                        System.out.println("Blockchain already exists");
                    }
                    BlockChain.this.onSyncComplete();
                });
            }
        }, 10*1000);
        //code
    }

    //function overloading - requesting already existing blockchain from the network
    @Override
    public void init(KeyPair keys, Context context) {
        consensusInterface.syncChain();
        // TODO: remove hardcoded value later
//        this.maxNumOfTransactions = 30;
//        this.keys = keys;
        //TODO: interact with consensus function to request blockchain from network & store in db
    }

    //TODO: Take the private, public keys from the device, rather than as an arg.
    @Override
    public void createTransaction(KeyPair keys, String sender, String receiver, Long amount, Map<String, String> data) {
        consensusInterface.createTransaction(keys, sender, receiver, amount, data, transactionValidator);
//        try {
//            Encryption encryption = new Encryption(keys);
//            String concatenatedString;
//            if (amount != null)
//                concatenatedString = sender + receiver + amount + data;
//            else
//                concatenatedString = sender + receiver + data;
//            byte[] byteArray = concatenatedString.getBytes(StandardCharsets.UTF_8);
//            String signature = encryption.sign(byteArray);
//            Transaction transaction = new Transaction(signature, encryption.getPubKeyString(), sender, receiver, amount, data);
//            if (this.transactionValidator != null) {
//                if (!this.transactionValidator.validate(transaction)) {
//                    // todo: refactor this to own error class
//                    //throw new RuntimeException("Invalid transaction");
//                    System.out.println("Invalid transaction");
//                }
//                addTransaction(transaction);
//            }
//
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
    }

    public void addTransaction(Transaction transaction) {
        // TODO: Make sure its a QUEUED transaction
        consensusInterface.addTransaction(transaction);
    }

    //TODO: Write a function to calculate merkle root
    //TODO: Remove public, private keys as arguments and fetch from device(?)
    @Override
    public void createBlock(Map<String, String> data, KeyPair keys, Context context) {
        //TODO: add "signature" field in the block
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            try {
                final String prevBlockHash;
                prevBlockHash = appDb.blockDao().getLastBlockHash();
                //TODO: read from DB - the number of transactions to be included in the block
                List<Transaction> queuedTransactions = appDb.transactionDao().getTransactions(TransactionStatus.QUEUED, this.maxNumOfTransactions);
                String merkleRoot = "";
                for (Transaction transaction : queuedTransactions) {
                    merkleRoot = StringUtil.applySha256(
                            merkleRoot + transaction.getHash()
                    );
                }

                String validator = Encryption.getAddress(keys);
                Encryption encryption = new Encryption(keys);
                String concatenatedString = prevBlockHash + validator + merkleRoot + data;
                byte[] byteArray = concatenatedString.getBytes(StandardCharsets.UTF_8);
                String signature = encryption.sign(byteArray);

                Block block = new Block(prevBlockHash, validator, signature, merkleRoot, data);
                Integer blockNum = appDb.blockDao().insertBlock(block, queuedTransactions);
                consensusInterface.sendNewBlock(appDb.blockDao().getBlock(blockNum));
                this.onBlockAdded(blockNum);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public List<Transaction> searchTransaction(CallbackInterface cb) {
        return null;
    }

    @Override
    public void searchInTransactionData(String query, TransactionListCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            List<Transaction> transactions = this.appDb.transactionDao().getTransactionsWithDataLike(query);
            cb.execute(transactions);
        });
    }

    public void getTransactionWithSenderAndData(String sender, String query, TransactionListCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            List<Transaction> transactions = this.appDb.transactionDao().getTransactionsWithSenderAndDataLike(sender, query);
            cb.execute(transactions);
        });
    }

    public void getTransactions(Integer numOfTransactions, TransactionListCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            List<Transaction> transactions = this.appDb.transactionDao().getTransactions(numOfTransactions);
            cb.execute(transactions);
        });
    }
    public void getTransactionWithReceiverAndData(String receiver, String query, TransactionListCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            List<Transaction> transactions = this.appDb.transactionDao().getTransactionsWithReceiverAndDataLike(receiver, query);
            cb.execute(transactions);
        });
    }

    public void getTransactionsCreatedAfterTime(Long time, TransactionListCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            List<Transaction> transactions = this.appDb.transactionDao().getTransactionsCreatedAfterTime(time);
            cb.execute(transactions);
        });
    }

    @Override
    public void getBlock(int num, BlockCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            BlockWithTransactions block = this.appDb.blockDao().getBlock(num);
            cb.execute(block);
        });
    }

    @Override
    public void getBlocks(int start, int end, BlockListCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            List<BlockWithTransactions> blocks = this.appDb.blockDao().getBlocksInRange(start, end);
            cb.execute(blocks);
        });
    }

    @Override
    public void searchInBlockData(String query, BlockListCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            List<BlockWithTransactions> blocks = this.appDb.blockDao().getBlocksWithDataLike(query);
            cb.execute(blocks);
        });
    }

    public void getBlockWithValidatorAndData(String validator, String query, BlockListCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            List<BlockWithTransactions> blocks = this.appDb.blockDao().getBlocksWithValidatorAndDataLike(validator, query);
            cb.execute(blocks);
        });
    }

    @Override
    public void getLastBlock(BlockCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            BlockWithTransactions block = this.appDb.blockDao().getLastBlock();
            cb.execute(block);
        });
    }

    public void checkIfAuthorityExists(String address, AuthorityCallback cb) {
        BlockchainDatabase.databaseWriteExecutor.execute(()->{
            //Boolean validatorExists = this.appDb.authorityNodeDao().checkIfAuthorityExists(address);
            //Boolean validatorExists = this.appDb.blockDao().
            cb.execute(true);
        });
    }

    @Override
    public void isValidChain() {
        consensusInterface.isValidChain();
    }

    // This is to sync with the chain ??? Check Later
    public void addBlock(Block block, List<Transaction> transactionsList) {
        consensusInterface.addBlock(block, transactionsList);
    }

    @Override
    public void addBlock(BlockWithTransactions block) {
        addBlock(block.block, block.transactions);
    }
}
