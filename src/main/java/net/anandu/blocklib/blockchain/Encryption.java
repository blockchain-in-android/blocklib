package net.anandu.blocklib.blockchain;

import android.util.Base64;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class Encryption {
    KeyPair key;

    public Encryption() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC");
        kpg.initialize(new ECGenParameterSpec("secp256r1"));
//        TODO: Remove Comment Later
//        If using Android KeyStore
//        kpg.initialize(new KeyGenParameterSpec.Builder(
//                "blockchain_library",
//                KeyProperties.PURPOSE_SIGN | KeyProperties.PURPOSE_VERIFY)
//                .setAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1"))
//                .setDigests(KeyProperties.DIGEST_SHA256,
//                        KeyProperties.DIGEST_SHA512)
//                .setUserAuthenticationRequired(true)
//                .setUserAuthenticationValidityDurationSeconds(5 * 60)
//                .build());
        this.key = kpg.generateKeyPair();
    }

    // TODO: Change to loading key through KeyStore for more security
    public Encryption(KeyPair key) {
        if (key.getPrivate().getAlgorithm().equals("EC"))
            this.key = key;
        else
            throw new IllegalArgumentException("Should be an EC Key");
    }

    public static KeyPair getKeyPairFromEncoded(String publicKey, String privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException {

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC");
        kpg.initialize(new ECGenParameterSpec("secp256r1"));
        byte[] decodedPubKeyBytes = Base64.decode(publicKey, Base64.DEFAULT);
        byte[] decodedPrivKeyBytes = Base64.decode(privateKey, Base64.DEFAULT);

        KeyFactory kf = KeyFactory.getInstance("EC");
        PublicKey pubKey = kf.generatePublic(new X509EncodedKeySpec(decodedPubKeyBytes));
        PrivateKey privKey = kf.generatePrivate(new PKCS8EncodedKeySpec(decodedPrivKeyBytes));

        return new KeyPair(pubKey, privKey);
    }

    public static String getAddress(KeyPair key) throws NoSuchAlgorithmException {
            byte[] pubKeyBytes = key.getPublic().getEncoded();

            // TODO: Change to SHA3-256 if possible
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] address = md.digest(pubKeyBytes);
            StringBuilder sb = new StringBuilder();
            for (byte b : address) {
                sb.append(String.format("%02x", b));
            }
            String pubKeyHash = sb.toString();
            return "0x" + pubKeyHash.substring(pubKeyHash.length() - 40);
    }

    public static boolean verify(byte[] message, String signature, String pubKeyString) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, InvalidKeySpecException {
        byte[] signatureBytes = Base64.decode(signature, Base64.DEFAULT);
        byte[] pubKeyBytes = Base64.decode(pubKeyString, Base64.DEFAULT);
        KeyFactory kf = KeyFactory.getInstance("EC");
        PublicKey pubKey = kf.generatePublic(new X509EncodedKeySpec(pubKeyBytes));
        Signature s = Signature.getInstance("SHA256withECDSA");
        s.initVerify(pubKey);
        s.update(message);
        return s.verify(signatureBytes);
    }

    public KeyPair getKey() {
        return this.key;
    }

    public String getPubKeyString() {
        byte[] pubKeyBytes = this.key.getPublic().getEncoded();
        return new String(Base64.encode(pubKeyBytes, Base64.DEFAULT));
    }

    public String getPrivateKeyString() {
        byte[] pubKeyBytes = this.key.getPrivate().getEncoded();
        return new String(Base64.encode(pubKeyBytes, Base64.DEFAULT));
    }

    public String sign(byte[] message) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature s = Signature.getInstance("SHA256withECDSA");
        s.initSign(key.getPrivate());
        s.update(message);
        return new String(Base64.encode(s.sign(), Base64.DEFAULT));
    }

    public boolean verify(byte[] message, String signature) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, InvalidKeySpecException {
        byte[] pubKeyBytes = Base64.encode(this.key.getPublic().getEncoded(), Base64.DEFAULT);
        return verify(message, signature, new String(pubKeyBytes));
    }
}
