package net.anandu.blocklib.blockchain;

import android.content.Context;

import net.anandu.blocklib.consensus.ConsensusInterface;
import net.anandu.blocklib.consensus.GenesisConfig;
import net.anandu.blocklib.database.Block;
import net.anandu.blocklib.database.BlockWithTransactions;
import net.anandu.blocklib.database.Transaction;
import net.anandu.blocklib.util.BlockCallback;
import net.anandu.blocklib.util.BlockListCallback;
import net.anandu.blocklib.util.CallbackInterface;
import net.anandu.blocklib.util.TransactionListCallback;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.List;
import java.util.Map;

public interface BlockChainInterface {
    void init(GenesisConfig config, KeyPair keys, Context context) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException;

    //function overloading - requesting already existing blockchain from the network
    void init(KeyPair keys, Context context);

    void createTransaction(KeyPair keys, String sender, String receiver, Long amount, Map<String, String> data);

    void createBlock(Map<String, String> data, KeyPair keys, Context context);

    void getBlock(int num, BlockCallback cb);

    void getLastBlock(BlockCallback cb);

    void getBlocks(int start, int end, BlockListCallback cb);

    void searchInBlockData(String query, BlockListCallback cb);

    void searchInTransactionData(String query, TransactionListCallback cb);

    List<Transaction> searchTransaction(CallbackInterface cb);

    void setConsensusMethod(ConsensusInterface c);

    void isValidChain();

    void addBlock(Block block, List<Transaction> transactionsList);

    void addBlock(BlockWithTransactions block);

    void onConnected();

    void onSyncComplete();

    void onBlockAdded(Integer blockNum);

    void onTransactionAdded(Transaction transaction);
}
