package net.anandu.blocklib.util;

import net.anandu.blocklib.database.BlockWithTransactions;

public interface BlockCallback {
    void execute(BlockWithTransactions block);
}
