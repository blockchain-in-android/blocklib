package net.anandu.blocklib.util;

public interface CallbackInterface {
    boolean execute();
}
