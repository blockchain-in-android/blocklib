package net.anandu.blocklib.util;

public interface AuthorityCallback {
    void execute(Boolean validatorExists);
}
