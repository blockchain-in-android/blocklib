package net.anandu.blocklib.util;

import net.anandu.blocklib.database.Transaction;

import java.util.List;

public interface TransactionListCallback {
    void execute(List<Transaction> transactions);
}
