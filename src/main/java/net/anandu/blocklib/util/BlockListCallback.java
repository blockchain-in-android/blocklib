package net.anandu.blocklib.util;

import net.anandu.blocklib.database.BlockWithTransactions;

import java.util.List;

public interface BlockListCallback {
    void execute(List<BlockWithTransactions> blocks);
}
