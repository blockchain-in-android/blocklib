package net.anandu.blocklib.database;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class BlockWithTransactions {
    @Embedded
    public Block block;

    @Relation(parentColumn = "blockNum", entityColumn = "blockNum", entity = Transaction.class)
    public List<Transaction> transactions;
}
