package net.anandu.blocklib.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

@Dao
public abstract class BelievabilityDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    abstract void insert(Believability believability);

    @Query("UPDATE Believability SET believabilityScore = believabilityScore + 1 WHERE address = :address")
    abstract int incrementQuery(String address);

    @Query("UPDATE Believability SET believabilityScore = believabilityScore - 1 WHERE address = :address")
    abstract int decrementQuery(String address);

    @Transaction
    public void increment(String address) {
        if (incrementQuery(address) == 0) {
            Believability believability = new Believability(address);
            insert(believability);
        }
    }

    @Transaction
    public void decrement(String address) {
        if (decrementQuery(address) == 0) {
            Believability believability = new Believability(address);
            insert(believability);
        }
    }
}
