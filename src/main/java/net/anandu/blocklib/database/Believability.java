package net.anandu.blocklib.database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Believability")
public class Believability {
    @PrimaryKey
    @NonNull
    private String address;
    private int believabilityScore;

    public Believability(@NonNull String address) {
        this.address = address;
        this.believabilityScore = 1;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    public void setAddress(@NonNull String address) {
        this.address = address;
    }

    @NonNull
    public Integer getBelievabilityScore() {
        return believabilityScore;
    }

    public void setBelievabilityScore(Integer believabilityScore) {
        this.believabilityScore = believabilityScore;
    }
}
