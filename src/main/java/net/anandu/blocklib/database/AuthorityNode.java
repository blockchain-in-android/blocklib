package net.anandu.blocklib.database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "AuthorityNodes")
public class AuthorityNode {
    @NonNull
    private final String address;
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    public AuthorityNode(@NonNull String address) {
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @NonNull
    public String getAddress() {
        return address;
    }
}
