package net.anandu.blocklib.database;

import java.io.Serializable;

public enum TransactionStatus implements Serializable {
    QUEUED,
    COMPLETED
}
