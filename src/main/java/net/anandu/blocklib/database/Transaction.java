package net.anandu.blocklib.database;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.Gson;

import net.anandu.blocklib.util.StringUtil;

import java.io.Serializable;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@Entity(tableName = "Transactions")
public class Transaction implements Serializable {
    @NonNull
    private final String digitalSign;
    @NonNull
    private final String publicKey;
    @NonNull
    private final String sender;
    @NonNull
    private final String receiver;
    private final Long amount;
    private final Map<String, String> data;
    @PrimaryKey
    @NonNull
    private String transactionId;
    @NonNull
    private String hash;
    @NonNull
    private Long timestamp;
    @Nullable
    private Integer blockNum;
    private TransactionStatus status;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Transaction(@NonNull String digitalSign, @NonNull String publicKey, @NonNull String sender, @NonNull String receiver, Long amount, Map<String, String> data) {
        this.transactionId = UUID.randomUUID().toString();
        this.amount = amount;
        this.data = data;
        this.timestamp = new Date().toInstant().getEpochSecond();
        this.digitalSign = digitalSign;
        this.publicKey = publicKey;
        this.sender = sender;
        this.receiver = receiver;
        this.status = TransactionStatus.QUEUED;
        this.hash = this.calculateHash();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Ignore
    public Transaction(@NonNull String digitalSign, @NonNull String publicKey, @NonNull String sender, @NonNull String receiver, Long amount, Map<String, String> data, TransactionStatus status) {
        this.transactionId = UUID.randomUUID().toString();
        this.amount = amount;
        this.data = data;
        this.timestamp = new Date().toInstant().getEpochSecond();
        this.digitalSign = digitalSign;
        this.publicKey = publicKey;
        this.sender = sender;
        this.receiver = receiver;
        this.status = status;
        this.hash = this.calculateHash();
    }

    public String calculateHash() {
        Gson gson = new Gson();
        String dataString = gson.toJson(data);
        return StringUtil.applySha256(
                this.transactionId +
                        this.sender +
                        this.receiver +
                        this.publicKey +
                        this.digitalSign +
                        this.timestamp +
                        this.amount +
                        dataString
        );
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    @NonNull
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@NonNull Long timestamp) {
        this.timestamp = timestamp;
    }

    @NonNull
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(@NonNull String transactionId) {
        this.transactionId = transactionId;
    }

    @Nullable
    public Integer getBlockNum() {
        return blockNum;
    }

    public void setBlockNum(@Nullable Integer blockNum) {
        this.blockNum = blockNum;
    }

    @NonNull
    public String getHash() {
        return hash;
    }

    public void setHash(@NonNull String hash) {
        this.hash = hash;
    }

    @NonNull
    public String getDigitalSign() {
        return digitalSign;
    }

    @NonNull
    public String getPublicKey() {
        return publicKey;
    }

    @NonNull
    public String getSender() {
        return sender;
    }

    @NonNull
    public String getReceiver() {
        return receiver;
    }

    @Nullable
    public Long getAmount() {
        return amount;
    }

    public Map<String, String> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "digitalSign='" + digitalSign + '\'' +
                ", publicKey='" + publicKey + '\'' +
                ", sender='" + sender + '\'' +
                ", receiver='" + receiver + '\'' +
                ", amount=" + amount +
                ", data=" + data +
                ", transactionId='" + transactionId + '\'' +
                ", hash='" + hash + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", blockNum=" + blockNum +
                ", status=" + status +
                '}';
    }
}
