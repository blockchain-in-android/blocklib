package net.anandu.blocklib.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TransactionDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Transaction transaction);

    @Delete
    void delete(Transaction transaction);

    @Query("SELECT * FROM Transactions ORDER BY timestamp DESC LIMIT :numOfTransactions")
    List<Transaction> getTransactions(Integer numOfTransactions);

    @Query("SELECT * FROM Transactions WHERE status=:transactionStatus ORDER BY timestamp DESC LIMIT :numOfTransactions")
    List<Transaction> getTransactions(TransactionStatus transactionStatus, Integer numOfTransactions);

    @Query("SELECT EXISTS(SELECT 1 FROM Transactions WHERE hash = :hash)")
    Boolean checkTransactionExists(String hash);

    @Query("SELECT * FROM Transactions WHERE data LIKE '%' || :query || '%' ORDER BY timestamp DESC")
    List<Transaction> getTransactionsWithDataLike(String query);

    @Query("SELECT * FROM Transactions WHERE sender = :sender AND data LIKE '%' || :query || '%'")
    List<Transaction> getTransactionsWithSenderAndDataLike(String sender, String query);

    @Query("SELECT * FROM Transactions WHERE receiver = :receiver AND data LIKE '%' || :query || '%'")
    List<Transaction> getTransactionsWithReceiverAndDataLike(String receiver, String query);

    @Query("SELECT * FROM Transactions WHERE timestamp > :time ORDER BY timestamp ASC")
    List<Transaction> getTransactionsCreatedAfterTime(Long time);
}
