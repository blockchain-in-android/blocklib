package net.anandu.blocklib.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Block.class, Transaction.class, AuthorityNode.class, Believability.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class BlockchainDatabase extends RoomDatabase {

    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private static volatile BlockchainDatabase INSTANCE;
    private static final RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };

    public static BlockchainDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (BlockchainDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            BlockchainDatabase.class, "blockchain_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract BlockDao blockDao();

    public abstract TransactionDao transactionDao();

    public abstract AuthorityNodeDao authorityNodeDao();

    public abstract BelievabilityDao believabilityDao();
}
