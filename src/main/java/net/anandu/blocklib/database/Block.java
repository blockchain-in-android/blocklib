package net.anandu.blocklib.database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.Gson;

import net.anandu.blocklib.util.StringUtil;

import java.util.Date;
import java.util.Map;

@Entity(tableName = "Blocks")
public class Block {
    @NonNull
    private final String prevHash;
    private final Map<String, String> data;
    @NonNull
    private final String merkleRoot;
    @NonNull
    private final String validator;
    @NonNull
    private final String signature;
    @NonNull
    private String currentHash;
    @PrimaryKey(autoGenerate = true)
    private Integer blockNum;
    @NonNull
    private String timestamp;

    public Block(@NonNull String prevHash, @NonNull String validator, @NonNull String signature, @NonNull String merkleRoot, Map<String, String> data) {
        this.prevHash = prevHash;
        this.validator = validator;
        this.signature = signature;
        this.merkleRoot = merkleRoot;
        this.data = data;
        this.timestamp = (new Date()).toString();
        this.currentHash = this.calculateHash();
    }

    public String calculateHash() {
        Gson gson = new Gson();
        String dataString = gson.toJson(data);
        return StringUtil.applySha256(
                this.prevHash +
                        this.validator +
                        this.merkleRoot +
                        this.timestamp +
                        dataString
        );
    }

    @NonNull
    public String getMerkleRoot() {
        return merkleRoot;
    }

    @NonNull
    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@NonNull String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getBlockNum() {
        return blockNum;
    }

    public void setBlockNum(Integer blockNum) {
        this.blockNum = blockNum;
    }

    @NonNull
    public String getCurrentHash() {
        return currentHash;
    }

    public void setCurrentHash(@NonNull String currentHash) {
        this.currentHash = currentHash;
    }

    @NonNull
    public String getValidator() {
        return validator;
    }

    @NonNull
    public String getSignature() {
        return signature;
    }

    @NonNull
    public String getPrevHash() {
        return prevHash;
    }

    public Map<String, String> getData() {
        return data;
    }
}
