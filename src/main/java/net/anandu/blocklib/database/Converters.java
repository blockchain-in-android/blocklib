package net.anandu.blocklib.database;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class Converters {
    @TypeConverter
    public static Map<String, String> toMap(String data) {
        Gson gson = new Gson();
        Type empMapType = new TypeToken<Map<String, String>>() {
        }.getType();
        return gson.fromJson(data, empMapType);
    }

    @TypeConverter
    public static String fromMap(Map<String, String> data) {
        Gson gson = new Gson();
        return gson.toJson(data);
    }

    @TypeConverter
    public static String fromTransactionStatus(TransactionStatus status) {
        return status.name();
    }

    @TypeConverter
    public static TransactionStatus toTransactionStatus(String status) {
        return TransactionStatus.valueOf(status);
    }
}
