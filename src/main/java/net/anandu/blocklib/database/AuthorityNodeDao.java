package net.anandu.blocklib.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface AuthorityNodeDao {
    @Insert
    void insert(AuthorityNode authorityNode);

    @Query("SELECT EXISTS(SELECT * FROM AuthorityNodes WHERE address = :address)")
    Boolean checkIfAuthorityExists(String address);
}
