package net.anandu.blocklib.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;


@Dao
public abstract class BlockDao {
    @androidx.room.Transaction
    public Integer insertBlock(Block block, List<Transaction> transactions) {
        Integer blockNum = (int) _insertBlockWithoutTransaction(block);
        for (Transaction transaction : transactions) {
            transaction.setBlockNum(blockNum);
            transaction.setStatus(TransactionStatus.COMPLETED);
        }
        // TODO: Change into Update maybe??
        _insertAllTransactions(transactions);
        return blockNum;
    }

    @androidx.room.Transaction
    public void insertBlock(List<BlockWithTransactions> blocks) {
        for (BlockWithTransactions block : blocks) {
            _insertBlockWithoutTransaction(block.block);
            _insertAllTransactions(block.transactions);
        }
    }

    @androidx.room.Transaction
    public void insertBlock(BlockWithTransactions block) {
        _insertBlockWithoutTransaction(block.block);
        _insertAllTransactions(block.transactions);
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract long _insertBlockWithoutTransaction(Block block);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract void _insertAllTransactions(List<Transaction> transactions);

    @Delete
    abstract void delete(Block block);

    @Query("SELECT * FROM Blocks")
    @androidx.room.Transaction
    public abstract LiveData<List<BlockWithTransactions>> getAllBlocks();

    @Query("SELECT * FROM Blocks WHERE blockNum=:blockNum")
    public abstract Block getBlockHeader(Integer blockNum);

    @Query("SELECT * FROM Blocks WHERE blockNum=:blockNum")
    @androidx.room.Transaction
    public abstract BlockWithTransactions getBlock(Integer blockNum);

    @Query("SELECT * FROM Blocks WHERE blockNum BETWEEN :start and :end")
    @androidx.room.Transaction
    public abstract List<BlockWithTransactions> getBlocksInRange(Integer start, Integer end);

    @Query("SELECT Count(*) FROM Blocks")
    public abstract long getBlocksCount();

    @Query("SELECT currentHash FROM Blocks ORDER BY blockNum DESC LIMIT 1")
    public abstract String getLastBlockHash();

    @Query("SELECT * FROM Blocks ORDER BY blockNum DESC LIMIT 1")
    @androidx.room.Transaction
    public abstract BlockWithTransactions getLastBlock();

    @Query("SELECT EXISTS(SELECT 1 FROM Blocks WHERE currentHash = :blockHash)")
    public abstract Boolean checkIfBlockExists(String blockHash);

    @Query("SELECT * FROM Blocks WHERE data LIKE '%' || :query || '%' AND NOT blockNum = 1")
    public abstract List<BlockWithTransactions> getBlocksWithDataLike(String query);

    @Query("SELECT * FROM Blocks WHERE validator = :validator AND data LIKE '%' || :query || '%' AND NOT blockNum = 1")
    public abstract List<BlockWithTransactions> getBlocksWithValidatorAndDataLike(String validator, String query);
}
