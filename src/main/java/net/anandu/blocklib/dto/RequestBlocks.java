package net.anandu.blocklib.dto;

public class RequestBlocks {
    private final Integer start;
    private final Integer end;

    public RequestBlocks(Integer start, Integer end) {
        this.start = start;
        this.end = end;
    }

    public Integer getStart() {
        return start;
    }

    public Integer getEnd() {
        return end;
    }
}
