package net.anandu.blocklib.dto;

import net.anandu.blocklib.util.StringUtil;

import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

// TODO: Remove this later, after replacing with everything with Block Entity
public class BlockDTO {
    public String hash;
    public String prevHash;
    private final Map<String, String> data;
    private final Long timeStamp;
    private final List<TransactionDTO> transactionList;

    public BlockDTO(Map<String, String> data, String prevHash, List<TransactionDTO> transactionList) {
        this.data = data;
        this.prevHash = prevHash;
        this.timeStamp = new Date().getTime();
        this.hash = calculateHash();
        this.transactionList = transactionList;
    }

    public String calculateHash() {
        String dataString = new JSONObject(this.data).toString();
        return StringUtil.applySha256(
                prevHash +
                        timeStamp +
                        dataString
        );
    }

    public Map<String, String> getData() {
        return data;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public List<TransactionDTO> getTransactionList() {
        return transactionList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlockDTO block = (BlockDTO) o;
        return timeStamp == block.timeStamp
                && Objects.equals(hash, block.hash)
                && Objects.equals(prevHash, block.prevHash)
                && Objects.equals(data, block.data);
    }
}