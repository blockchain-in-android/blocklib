package net.anandu.blocklib.dto;

import net.anandu.blocklib.util.StringUtil;

import java.util.Date;
import java.util.Map;

// TODO: Remove this later, after replacing with everything with Transaction Entity
public class TransactionDTO {
    private final Map<String, String> data;
    private final long timeStamp;

    public TransactionDTO(Map<String, String> data) {
        this.data = data;
        this.timeStamp = new Date().getTime();
    }

    public String calculateHash() {
        return StringUtil.applySha256(
                Long.toString(timeStamp) +
                        data
        );
    }

    public Map<String, String> getData() {
        return data;
    }
}
