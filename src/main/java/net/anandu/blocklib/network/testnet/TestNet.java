package net.anandu.blocklib.network.testnet;

import android.util.Log;

import com.google.gson.Gson;

import net.anandu.blocklib.network.NetworkInterface;
import net.anandu.blocklib.network.NetworkPayload;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class TestNet implements NetworkInterface {
    private static final String TAG = "TestNet";
    private static final String SOCKET_URL = "ws://103.170.132.165:8999/";
    private WebSocketClient client;
    private final String networkId;
    private final String peerId;

    public TestNet(String networkId, String peerId) {
        this.networkId = networkId;
        this.peerId = peerId;

        try {
            client = new WebSocketClient(new URI(SOCKET_URL)) {
                @Override
                public void onOpen(ServerHandshake handshakeData) {
                    Log.d(TAG, "onOpen: Connection established");
                }

                @Override
                public void onMessage(String message) {
                    TestNet.this.processMessage(message);
                }

                @Override
                public void onClose(int code, String reason, boolean remote) {
                    Log.d(TAG, "onClose: code" + code + " reason " + reason);
                }

                @Override
                public void onError(Exception ex) {
                    Log.e(TAG, "error" + ex.toString());
                }
            };
        } catch (URISyntaxException ue) {
            Log.e(TAG, "TestNet: Invalid uri" + SOCKET_URL);
        }

    }

    @Override
    public boolean connect() {
        if (client != null) {
            client.connect();
            return true;
        }
        return false;
    }

    @Override
    public boolean broadcast(String message) {
        if (client != null) {
            NetworkPayload networkPayload = new NetworkPayload(
                    "broadcast",
                    message,
                    peerId,
                    networkId
            );
            client.send(new Gson().toJson(networkPayload));
            return true;
        }
        return false;
    }

    @Override
    public boolean message(String toId, String type, String message) {
        if (client != null) {
            NetworkPayload networkPayload = new NetworkPayload(
                    type,
                    message,
                    toId,
                    peerId,
                    networkId
            );
            client.send(new Gson().toJson(networkPayload));
            return true;
        }
        return false;
    }

    private void processMessage(String packet) {
        System.out.println(packet);
        NetworkPayload content = new Gson().fromJson(packet, NetworkPayload.class);
        if (!this.networkId.equals(content.getNetworkId())) {
            Log.d(TAG, "onMessageReceived: Invalid networkId" + networkId);
            return;
        }
        if ("broadcast".equals(content.getType())) {
            this.onMessageReceived(content);
            return;
        }
        if ("peer_message".equals(content.getType())
                && content.getToId() != null
                && peerId.equals(content.getToId())) {
            this.onMessageReceived(content);
            return;
        }

        Log.d(TAG, "processMessage: invalid message" + content);

    }

    @Override
    public void onMessageReceived(NetworkPayload message) {
        Log.d(TAG, "onMessageReceived: " + message);

    }
}
