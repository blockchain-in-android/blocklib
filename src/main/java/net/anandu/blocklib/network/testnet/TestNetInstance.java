package net.anandu.blocklib.network.testnet;

import net.anandu.blocklib.network.NetworkPayload;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class TestNetInstance {

    private static TestNet instance;
    private static final ArrayList<Listener> listeners;

    static {
        listeners = new ArrayList<>();
    }

    private TestNetInstance() {
    }

    public static void configure(String networkId, String peerId) {
        instance = new TestNet(networkId, peerId) {
            @Override
            public void onMessageReceived(NetworkPayload message) {
                TestNetInstance.handleMessage(message);
            }
        };
        instance.connect();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void handleMessage(NetworkPayload message) {
        for (Listener l : listeners) {
            l.execute(message);
        }
    }

    public static void addListener(Listener listener) {
        listeners.add(listener);
    }

    public static TestNet getInstance() {
        if (instance == null) {
            throw new RuntimeException("Instance not configured");
        }
        return instance;
    }

    public interface Listener {
        void execute(NetworkPayload n);
    }


}
