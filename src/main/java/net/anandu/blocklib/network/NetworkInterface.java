package net.anandu.blocklib.network;

public interface NetworkInterface {
    boolean connect();

    boolean broadcast(String message);

    boolean message(String peerId, String type, String message);

    void onMessageReceived(NetworkPayload message);
}
