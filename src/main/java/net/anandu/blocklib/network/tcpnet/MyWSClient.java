package net.anandu.blocklib.network.tcpnet;


import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;

public class MyWSClient extends WebSocketClient {

    public MyWSClient(URI serverUri, Draft draft) {
        super(serverUri, draft);
    }

    public MyWSClient(URI serverURI) {
        super(serverURI);
    }

    public static void main(String[] args) throws URISyntaxException {
        WebSocketClient client = new MyWSClient(new URI("ws://localhost:8887"));
        client.connect();
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        send("Hello, it is me. Mario :)");
        System.out.println("new connection opened");
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        System.out.println("closed with exit code " + code + " additional info: " + reason);
    }

    @Override
    public void onMessage(String message) {
        System.out.println("received message: " + message);
    }

    @Override
    public void onMessage(ByteBuffer message) {
        System.out.println("received ByteBuffer");
    }

    @Override
    public void onError(Exception ex) {
        System.err.println("an error occurred:" + ex);
    }
}
