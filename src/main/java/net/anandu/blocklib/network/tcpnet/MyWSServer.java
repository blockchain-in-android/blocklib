package net.anandu.blocklib.network.tcpnet;


import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class MyWSServer extends WebSocketServer {

    public MyWSServer(int port) throws UnknownHostException {
        super(new InetSocketAddress(port));
    }

    public MyWSServer(InetSocketAddress address) {
        super(address);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        System.out.println("Connection opened");
        System.out.println("New connection: " + conn.getRemoteSocketAddress());
//        conn.send("Hi there from the server ");

    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("Connection closed");
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        System.out.println("Message received: " + message);

    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        System.out.println("Error " + ex.getMessage());

    }
}