package net.anandu.blocklib.network.tcpnet;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;
import android.widget.Toast;

import net.anandu.blocklib.network.NetworkInterface;
import net.anandu.blocklib.network.NetworkPayload;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.server.WebSocketServer;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.UUID;

public class TCPNet implements NetworkInterface {

    private static final String TAG = "LG1";
    static int trackerPort = 8085;
    WebSocketServer ws = null;
    Context applicationContext = null; // todo: load this value from top
    String SERVICE_NAME = "NSDTest";
    String SERVICE_TYPE = "_nsdtest._tcp";

    @Override
    public boolean connect() {
        SERVICE_NAME = SERVICE_NAME + UUID.randomUUID();
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d("nsd-test", "Starting WSServer on " + trackerPort);
                    ws = new MyWSServer(trackerPort);
                    ws.start();
                    Log.d("nsd-test", "WebSocket server started at " + trackerPort);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(myRunnable).start();
        discover();
        return false;
    }

    public void addTracker(String trackerURL) throws URISyntaxException {
        Log.d("nsd-test", "Got tracker URL" + trackerURL);
        Toast.makeText(applicationContext, "Got new tracker HERE\n" + trackerURL, Toast.LENGTH_LONG).show();
        WebSocketClient myWSClient = new MyWSClient(new URI(trackerURL));
        myWSClient.connect();
    }

    private void discover() {
        registerTrackerService();

        // ----
        // Discover trackers in network
        // ----

        final NsdManager nsdManager = (NsdManager) applicationContext.getSystemService(Context.NSD_SERVICE);

        final NsdManager.ResolveListener resolveListener = new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Called when the resolve fails. Use the error code to debug.
                Log.e("nsd-test", "Resolve failed: " + errorCode);
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                Log.e("nsd-test", "Resolve Succeeded. " + serviceInfo);

                if (serviceInfo.getServiceName().equals(SERVICE_NAME)) {
                    Log.d("nsd-test", "Same IP.");
                    return;
                }
                NsdServiceInfo mService = serviceInfo;
                int port = mService.getPort();
                InetAddress host = mService.getHost();

                Log.d("nsd-test", "Found device at " + host + ":" + port);
                try {
                    addTracker("ws:/" + host + ":" + port);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        };

        // Instantiate a new DiscoveryListener
        NsdManager.DiscoveryListener discoveryListener = new NsdManager.DiscoveryListener() {

            // Called as soon as service discovery begins.
            @Override
            public void onDiscoveryStarted(String regType) {
                Log.d("nsd-test", "Service discovery started");
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                // A service was found! Do something with it.
                Log.d("nsd-test", "Service discovery success" + service);
                if (!service.getServiceType().startsWith(SERVICE_TYPE)) {
                    // Service type is the string containing the protocol and
                    // transport layer for this service.
                    Log.d("nsd-test", "Unknown Service Type: " + service.getServiceType() + " Expected: " + SERVICE_TYPE);
                } else if (service.getServiceName().equals(SERVICE_NAME)) {
                    // The name of the service tells the user what they'd be
                    // connecting to. It could be "Bob's Chat App".
                    Log.d("nsd-test", "Same machine: " + SERVICE_NAME);
                } else if (service.getServiceName().contains("NSDTest")) {
                    Log.d("nsd-test", "Starting service resolution");
                    nsdManager.resolveService(service, resolveListener);
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                // When the network service is no longer available.
                // Internal bookkeeping code goes here.
                Log.e("nsd-test", "service lost: " + service);
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i("nsd-test", "Discovery stopped: " + serviceType);
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.e("nsd-test", "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.e("nsd-test", "Discovery failed: Error code:" + errorCode);
                nsdManager.stopServiceDiscovery(this);
            }
        };

        nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener);
    }

    private void registerTrackerService() {
        // Register our tracker service

        NsdManager.RegistrationListener registrationListener = new NsdManager.RegistrationListener() {
            @Override
            public void onServiceRegistered(NsdServiceInfo NsdServiceInfo) {
                // Save the service name. Android may have changed it in order to
                // resolve a conflict, so update the name you initially requested
                // with the name Android actually used.
                String serviceName = NsdServiceInfo.getServiceName();
            }

            @Override
            public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Registration failed! Put debugging code here to determine why.
            }

            @Override
            public void onServiceUnregistered(NsdServiceInfo arg0) {
                // Service has been unregistered. This only happens when you call
                // NsdManager.unregisterService() and pass in this listener.
            }

            @Override
            public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Unregistration failed. Put debugging code here to determine why.
            }
        };

        // Create the NsdServiceInfo object, and populate it.
        NsdServiceInfo serviceInfo = new NsdServiceInfo();

        // The name is subject to change based on conflicts
        // with other services advertised on the same network.
        serviceInfo.setServiceName(SERVICE_NAME);
        serviceInfo.setServiceType(SERVICE_TYPE);
        serviceInfo.setPort(trackerPort);

        NsdManager nsdManager = (NsdManager) applicationContext.getSystemService(Context.NSD_SERVICE);

        nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, registrationListener);
    }

    @Override
    public boolean broadcast(String message) {
        // use this.websoket server to broadcast and message
        return false;
    }

    @Override
    public boolean message(String peerId, String type, String message) {
        return false;
    }

    @Override
    public void onMessageReceived(NetworkPayload message) {

    }
}
