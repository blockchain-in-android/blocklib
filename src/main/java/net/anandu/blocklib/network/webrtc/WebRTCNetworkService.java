package net.anandu.blocklib.network.webrtc;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import net.anandu.blocklib.network.webrtc.payload.ConnectionRequest;
import net.anandu.blocklib.network.webrtc.payload.OfferResponse;

import org.java_websocket.handshake.ServerHandshake;
import org.webrtc.MediaConstraints;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SessionDescription;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class WebRTCNetworkService {
    private static final int MAX_CONNECTIONS = 1;
    Context context;
    PeerConnectionFactory factory;
    MediaConstraints constraints;
    List<PeerConnection.IceServer> iceServers;
    Map<String, String> offers = new HashMap<>();
    Map<String, MyPeerConnection> pendingConnections;
    Map<String, MyPeerConnection> connections;
    SignalClient signalClient;
    int numHave = 0;
    int numWant = 0;
    private final String peerId;

    public WebRTCNetworkService(Context c) {
        peerId = UUID.randomUUID().toString();
        if (numWant == 0) numWant = MAX_CONNECTIONS;

        this.context = c;
        pendingConnections = new HashMap<>();
        connections = new HashMap<>();

        // -- WebRTC setup
        PeerConnectionFactory.initialize(PeerConnectionFactory.InitializationOptions.builder(c).createInitializationOptions());
        PeerConnectionFactory factory = PeerConnectionFactory.builder().createPeerConnectionFactory();
        MediaConstraints constraints = new MediaConstraints();
        constraints.optional.add(new MediaConstraints.KeyValuePair("offerToReceiveAudio", "false"));
        constraints.optional.add(new MediaConstraints.KeyValuePair("offerToReceiveVideo", "false"));
        constraints.optional.add(new MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"));

        List<PeerConnection.IceServer> iceServers = new ArrayList<>();

//        PeerConnection.IceServer.Builder iceServerBuilder = PeerConnection.IceServer.builder("stun:stun.l.google.com:19302");
//        iceServerBuilder.setTlsCertPolicy(PeerConnection.TlsCertPolicy.TLS_CERT_POLICY_INSECURE_NO_CHECK); //this does the magic.
//        PeerConnection.IceServer iceServer = iceServerBuilder.createIceServer();
//        iceServers.add(iceServer);
        PeerConnection.IceServer.Builder iceServerBuilder2 = PeerConnection.IceServer.builder("stun:stun.nextcloud.com:443");
        iceServerBuilder2.setTlsCertPolicy(PeerConnection.TlsCertPolicy.TLS_CERT_POLICY_INSECURE_NO_CHECK); //this does the magic.
        PeerConnection.IceServer iceServer2 = iceServerBuilder2.createIceServer();
        iceServers.add(iceServer2);

        this.factory = factory;
        this.constraints = constraints;
        this.iceServers = iceServers;

        try {

            SignalClient client = new SignalClient(new URI("ws://103.170.132.165:8999/")) {
                @Override
                public void onMessage(String message) {
                    // parse the message
                    JsonObject content = new Gson().fromJson(message, JsonObject.class);
                    String type = content.get("type").getAsString();
                    if ("junk".equals(type)) {

                    } else if ("CONNECTION_REQUEST".equals(type)) {
                        ConnectionRequest request = new Gson().fromJson(message, ConnectionRequest.class);
                        WebRTCNetworkService.this.handlePeerRequest(request);
                    } else if ("ANSWER".equals(type)) {
                        OfferResponse r = new Gson().fromJson(message, OfferResponse.class);
                        WebRTCNetworkService.this.handleAnswerReceived(r.getOfferId(), r.getPeerId(), r.getRemoteDesc());
                    } else {
                        System.out.println(content);
                        System.out.println("GOT" + message);
                    }
                }

                @Override
                public void onOpen(ServerHandshake serverHandshake) {
                    send("{\"type\": \"junk\"}");
                    System.out.println("new connection opened");
                    WebRTCNetworkService.this.connect();
                }
            };

            client.connect();
            this.signalClient = client;
        } catch (URISyntaxException ue) {
            System.out.println("Invalid uRL");
        }

    }

    private void handleReadyRequest() {

        // called when all connection instances are setup
//        Map<String, String> offers = new HashMap<>();
//        for (Map.Entry<String, MyPeerConnection> entry : pendingConnections.entrySet()) {
//            offers.put(entry.getKey(), entry.getValue().getLocalDesc().description);
//        }
        ConnectionRequest request = new ConnectionRequest();
        request.setNumWant(this.numWant);
        request.setOffers(this.offers);
        request.setPeerId(this.peerId);
        // call the signalling server
        // then we'll build and send the request to signalling server

        // todo: send through signalling server
        this.signalClient.send(new Gson().toJson(request));
    }

    private void handlePeerRequest(ConnectionRequest request) {
        // unpack, generate a random ID
        if (request.getPeerId().equals(peerId)) {
            System.out.println("got message from self");
            return;
        }

        List<String> keysAsArray = new ArrayList<String>(request.getOffers().keySet());
        Random r = new Random();
        String remoteOfferId = keysAsArray.get(r.nextInt(keysAsArray.size()));
        String remoteOffer = request.getOffers().get(remoteOfferId);
        SessionDescription remoteSessionDesc = new SessionDescription(SessionDescription.Type.OFFER, remoteOffer);
        System.out.println("Responding to offerId" + remoteOfferId);
        // create a new PeerConnection with remote Description
        // and send it's answer back
        MyPeerConnection p = new MyPeerConnection(
                factory,
                constraints,
                iceServers,
                remoteSessionDesc,
                m -> {
                    Log.d("ANANDU", "Received message from peer connection: " + m);
                },
                sessionDescription -> {
                    OfferResponse ans = new OfferResponse();
                    ans.setOfferId(remoteOfferId);
                    ans.setPeerId(WebRTCNetworkService.this.peerId);
                    ans.setRemoteDesc(sessionDescription.description);
//                    System.out.println(ans);
                    System.out.println("Sending answer through signal client");
                    WebRTCNetworkService.this.signalClient.send(new Gson().toJson(ans));
                    // todo: send the answer here through signalling server
                }
        );
        pendingConnections.put(remoteOfferId, p);
        // create a peerConnection, wait for localDesc
        // then make a new peerConnection
        // attach peerDescription[randomId]
        // push the connection to peers with peerId
        // send answer through signalling client
    }

    private void handleAnswerReceived(String offerId, String remotePeerId, String responseSDP) {
        // check if connection is in pendingConnections
        if (remotePeerId.equals(peerId)) {
            System.out.println("Peer Id is the same");
            return;
        }
        if (!pendingConnections.containsKey(offerId)) {
            System.out.println("Connected already accepted");
            return;
        }
        MyPeerConnection connection = pendingConnections.remove(offerId);
        assert connection != null;
        connection.handleAnswer(remotePeerId, responseSDP);
        connections.put(remotePeerId, connection);
        // if yes, pop it off and give SDP string  to corresponding instance
        // push to available connections identify by PeerID
    }


    public boolean connect() {
        System.out.println("Connecting ");
        for (int i = 0; i < numWant; i++) {
            String offerId = "Offer_" + UUID.randomUUID().toString();
            MyPeerConnection conn = new MyPeerConnection(
                    this.factory, this.constraints, this.iceServers, this.peerId,
                    (message) -> {
                        System.out.println("got message: " + message);
//                        this.onMessageReceived(message);
                    },
                    (offer) -> {
                        numHave++;
                        // store offerId + offer
                        System.out.println("OfferID: " + offerId + " resolved" + offer.description);
                        System.out.println("numWant: " + numWant);
                        System.out.println("numHave: " + numHave);
                        offers.put(offerId, offer.description);
                        if (numHave >= numWant) {
                            this.handleReadyRequest();
                        }
                    }
            );
            pendingConnections.put(offerId, conn);
        }
        return false;
    }

    public boolean broadcast(String message) {
        System.out.println("Broadcasting message");
        System.out.println(pendingConnections);
        System.out.println(connections);
        System.out.println(peerId);

        for (Map.Entry<String, MyPeerConnection> entry : connections.entrySet()) {
//            offers.put(entry.getKey(), entry.getValue().getLocalDesc().description);
            MyPeerConnection c = entry.getValue();
            c.printDataChannel();

            c.sendMessage("I am sending from " + peerId + message);
        }

        for (Map.Entry<String, MyPeerConnection> entry : pendingConnections.entrySet()) {
//            offers.put(entry.getKey(), entry.getValue().getLocalDesc().description);
            MyPeerConnection c = entry.getValue();
            System.out.println("Sending ");
            c.printDataChannel();
            c.sendMessage("I am sending from " + peerId + message);
            System.out.println("Done");
        }
        return false;
    }

//    @Override
//    public boolean message(String peerId, String message) {
//        return false;
//    }

//    @Override
//    public void onMessageReceived(NetworkPayload message) {
//
//    }


}
