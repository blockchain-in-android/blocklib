package net.anandu.blocklib.network.webrtc;

import android.util.Log;

import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SessionDescription;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class MyPeerConnection implements DataChannel.Observer {

    private final ScheduledExecutorService executor;
    private String localPeerId;
    private String remotePeerId;
    private SessionDescription localDesc;
    private SessionDescription remoteDesc;


    private PeerConnection connection;
    private DataChannel dataChannel;
    private final OnMessageHandler messageHandler;
    private OnOfferHandler offerHandler;
    private OnAnswerHandler answerHandler;

    public MyPeerConnection(PeerConnectionFactory peerConnectionFactory,
                            MediaConstraints constraints,
                            List<PeerConnection.IceServer> iceServerList,
                            String localPeerId,
                            OnMessageHandler messageHandler,
                            OnOfferHandler offerHandler
    ) {

        this.localPeerId = localPeerId;
        this.messageHandler = messageHandler;
        this.offerHandler = offerHandler;
        executor = Executors.newSingleThreadScheduledExecutor();

        executor.execute(() -> {
            connection = peerConnectionFactory.createPeerConnection(iceServerList, new DefaultObserver() {
                @Override
                public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
                    super.onIceGatheringChange(iceGatheringState);
                    if (iceGatheringState == PeerConnection.IceGatheringState.COMPLETE) {
                        try {
//                            System.out.println("Connection:" + connection.getLocalDescription().description);
                            if (MyPeerConnection.this.offerHandler != null) {
//                                MyPeerConnection.this.localDesc = connection.getLocalDescription();
                                MyPeerConnection.this.setLocalDesc(connection.getLocalDescription());
                                MyPeerConnection.this.offerHandler.handleOffer(connection.getLocalDescription());

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
                    super.onIceConnectionChange(iceConnectionState);
//                    System.out.println("State changed" + iceConnectionState);
                }

                @Override
                public void onAddStream(MediaStream mediaStream) {
                    super.onAddStream(mediaStream);
                }

                @Override
                public void onDataChannel(DataChannel dataChannel) {
//                    super.onDataChannel(dataChannel);
                    System.out.println("DataChannel setup");
                    MyPeerConnection.this.dataChannel = dataChannel;
                    dataChannel.registerObserver(MyPeerConnection.this);
                }
            });

            assert connection != null;

            // test2
//            DataChannel.Init dcInit = new DataChannel.Init();
//            dcInit.id = 1;
//            this.dataChannel = connection.createDataChannel("1", dcInit);;
//            this.dataChannel.registerObserver(this);

            this.dataChannel = connection.createDataChannel("data", new DataChannel.Init());
//            this.dataChannel.registerObserver(this);
            connection.createOffer(new DefaultSdpObserver() {
                @Override
                public void onCreateSuccess(SessionDescription sessionDescription) {
                    super.onCreateSuccess(sessionDescription);
                    MyPeerConnection.this.setLocalDesc(connection.getLocalDescription());
                    connection.setLocalDescription(new DefaultSdpObserver(), sessionDescription);
                }
            }, constraints);

        });
    }

    public MyPeerConnection(PeerConnectionFactory peerConnectionFactory,
                            MediaConstraints constraints,
                            List<PeerConnection.IceServer> iceServerList,
                            SessionDescription offer,
                            OnMessageHandler messageHandler,
                            OnAnswerHandler answerHandler) {

        executor = Executors.newSingleThreadScheduledExecutor();

        this.messageHandler = messageHandler;
        this.answerHandler = answerHandler;


        executor.execute(() -> {
            log("Creating connection");
            MyPeerConnection.this.connection = peerConnectionFactory.createPeerConnection(iceServerList, new DefaultObserver() {
                @Override
                public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
                    super.onIceGatheringChange(iceGatheringState);
//                    log("Ice gathering state change: " + iceGatheringState);
                    if (iceGatheringState == PeerConnection.IceGatheringState.COMPLETE) {
                        System.out.println("Ice gathering complete");
//                        System.out.println("Creating answer");
                        try {
                            System.out.println("Ice gathering for answer completed");
//                            MyPeerConnection.this.answerHandler.handleAnswer(connection.getLocalDescription());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }


                @Override
                public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
                    log("creating answer ice change: " + iceConnectionState);
                    super.onIceConnectionChange(iceConnectionState);
                    if (iceConnectionState == PeerConnection.IceConnectionState.DISCONNECTED) {
//                        System.out.println("creating answer ice change: ENDED?");
                    }
                }

                @Override
                public void onAddStream(MediaStream mediaStream) {
                    super.onAddStream(mediaStream);
                }


                @Override
                public void onDataChannel(DataChannel dataChannel) {
                    super.onDataChannel(dataChannel);
                    System.out.println("Data channel observer done");
                    MyPeerConnection.this.dataChannel = dataChannel;
                    dataChannel.registerObserver(MyPeerConnection.this);
                }

            });
            assert connection != null;
            log("Connection created");

//            DataChannel.Init dcInit = new DataChannel.Init();
//            dcInit.id = 1;
//            this.dataChannel = connection.createDataChannel("1", dcInit);;
//            this.dataChannel.registerObserver(this);


            this.dataChannel = connection.createDataChannel("data", new DataChannel.Init());
//            this.dataChannel.registerObserver(this);
            log("DataChannel setup");
//            log("local sessiondescription : " + offer.description);

            connection.setRemoteDescription(new DefaultSdpObserver() {
                @Override
                public void onSetSuccess() {
                    super.onSetSuccess();
//                    log("SDPOBVERSERVER: creating answer...");
                    //-- answer
                    connection.createAnswer(new DefaultSdpObserver() {
                        @Override
                        public void onCreateSuccess(SessionDescription sessionDescription) {
//                            log("SDPOBVERSERVER: success");
//                            super.onCreateSuccess(sessionDescription);
//                            log("local sessiondescription : " + sessionDescription.description);
//                            System.out.println("OBSERVER:" + );
                            System.out.println(sessionDescription.type + " " + sessionDescription.description);
                            System.out.println("Answer created, sending answer");
                            MyPeerConnection.this.connection.setLocalDescription(new DefaultSdpObserver(), sessionDescription);
                            MyPeerConnection.this.answerHandler.handleAnswer(sessionDescription);
                        }

                        @Override
                        public void onCreateFailure(String s) {
                            super.onCreateFailure(s);
                            log("SDPOBSERVER: failure: " + s);
                        }

                        @Override
                        public void onSetFailure(String s) {
                            System.out.println("SDPOBSERVERREMOTE: Set failed" + s);
                            System.out.println("OFFER: " + offer.description + "ENDOFFER");
                        }
                    }, constraints);
                }
            }, offer);

            log("Remote description set");

        });
    }

    public SessionDescription getLocalDesc() {
        return localDesc;
    }

    public void setLocalDesc(SessionDescription localDesc) {
        this.localDesc = localDesc;
    }

    public String getLocalPeerId() {
        return localPeerId;
    }

    public void setLocalPeerId(String localPeerId) {
        this.localPeerId = localPeerId;
    }

    public String getRemotePeerId() {
        return remotePeerId;
    }

    public void setRemotePeerId(String remotePeerId) {
        this.remotePeerId = remotePeerId;
    }

    private void log(String message) {
        Log.d("PeerConnection", message);
    }

    public void sendMessage(String message) {
        executor.execute(() -> {
            System.out.println("Sending inside MyPeerConneciton");
            dataChannel.send(new DataChannel.Buffer(ByteBuffer.wrap(message.getBytes()), false));
            System.out.println("Sending done inside MyPeerConneciton");
        });
    }

    public void handleAnswer(String remotePeerId, String remoteDesc) {
        this.setRemotePeerId(remotePeerId);
        executor.execute(() -> {
            connection.setRemoteDescription(new DefaultSdpObserver() {
                @Override
                public void onSetSuccess() {
                    super.onSetSuccess();
                    MyPeerConnection.this.remoteDesc = connection.getRemoteDescription();
                    System.out.println("success in answering");
//                MyPeerConnection.this.sendMessage("HI through WebRTC");
                }

                @Override
                public void onSetFailure(String s) {
                    super.onSetFailure(s);
                    System.out.println("failure: " + s);
                }
            }, new SessionDescription(SessionDescription.Type.ANSWER, remoteDesc));
        });
    }

    public void handleIceCandidate(IceCandidate iceCandidate) {
        log("Added Ice candidate");
        connection.addIceCandidate(iceCandidate);
    }

    public void printDataChannel() {
        if (Objects.isNull(this.dataChannel)) {
            System.out.println("DataChannel is null :(");
            return;
        }
        System.out.println("DataChannel--");
        System.out.println("ConnecitonState:" + connection.connectionState());
        System.out.println(this.dataChannel.id());
        System.out.println(this.dataChannel.state());
        System.out.println("DataChannel--");
    }

    // overrides of dataChannel Observer
    @Override
    public void onBufferedAmountChange(long l) {
        System.out.println("Buffered amount changed" + l);
    }

    @Override
    public void onStateChange() {
        System.out.println("Data channel state changed" + dataChannel.state());
    }

    @Override
    public void onMessage(DataChannel.Buffer buffer) {
        byte[] data = new byte[buffer.data.remaining()];
        buffer.data.get(data);
        String s = new String(data);
        System.out.println("got message through channel" + s);
        if (this.messageHandler != null) {
            this.messageHandler.handleMessage(s);
        }
    }

    public interface OnMessageHandler {
        void handleMessage(String message);
    }

    public interface OnOfferHandler {
        void handleOffer(SessionDescription offer);
    }

    public interface OnAnswerHandler {
        void handleAnswer(SessionDescription offer);
    }


}
