package net.anandu.blocklib.network.webrtc;

import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.RtpReceiver;

import java.util.Arrays;

public class DefaultObserver implements PeerConnection.Observer {
    @Override
    public void onSignalingChange(PeerConnection.SignalingState signalingState) {
        System.out.println("Signalling State changed: " + signalingState);

    }

    @Override
    public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
        System.out.println("Ice connection changed: " + iceConnectionState);
    }

    @Override
    public void onIceConnectionReceivingChange(boolean b) {
        System.out.println("Ice connection  receiving changed: " + b);
    }

    @Override
    public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
        System.out.println("Ice gathering change: " + iceGatheringState);

    }

    @Override
    public void onIceCandidate(IceCandidate iceCandidate) {
        System.out.println("On Ice Candidate" + iceCandidate);

    }

    @Override
    public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {
        System.out.println("Ice candidate removed" + Arrays.toString(iceCandidates));

    }

    @Override
    public void onAddStream(MediaStream mediaStream) {

    }

    @Override
    public void onRemoveStream(MediaStream mediaStream) {

    }

    @Override
    public void onDataChannel(DataChannel dataChannel) {
        System.out.println("Data channel added" + dataChannel);

    }

    @Override
    public void onRenegotiationNeeded() {

    }

    @Override
    public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {

    }
}
