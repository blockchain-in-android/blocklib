package net.anandu.blocklib.network.webrtc.payload;

import java.util.Map;

public class ConnectionRequest {
    private final String type = "CONNECTION_REQUEST";
    private String peerId;
    private Integer numWant;
    private Map<String, String> offers;

    public ConnectionRequest() {
    }

    public ConnectionRequest(String peerId, Integer numWant, Map<String, String> offers) {
        this.peerId = peerId;
        this.numWant = numWant;
        this.offers = offers;
    }

    public String getPeerId() {
        return peerId;
    }

    public void setPeerId(String peerId) {
        this.peerId = peerId;
    }

    public Integer getNumWant() {
        return numWant;
    }

    public void setNumWant(Integer numWant) {
        this.numWant = numWant;
    }

    public Map<String, String> getOffers() {
        return offers;
    }

    public void setOffers(Map<String, String> offers) {
        this.offers = offers;
    }

    public String getType() {
        return type;
    }
}
