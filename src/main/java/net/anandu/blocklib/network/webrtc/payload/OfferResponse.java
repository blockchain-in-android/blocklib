package net.anandu.blocklib.network.webrtc.payload;

public class OfferResponse {
    private final String type = "ANSWER";
    private String peerId;
    private String offerId;
    private String remoteDesc;

    public OfferResponse() {
    }

    public OfferResponse(String peerId, String offerId, String remoteDesc) {
        this.peerId = peerId;
        this.offerId = offerId;
        this.remoteDesc = remoteDesc;
    }

    public String getPeerId() {
        return peerId;
    }

    public void setPeerId(String peerId) {
        this.peerId = peerId;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getRemoteDesc() {
        return remoteDesc;
    }

    public void setRemoteDesc(String remoteDesc) {
        this.remoteDesc = remoteDesc;
    }

    public String getType() {
        return type;
    }
}
