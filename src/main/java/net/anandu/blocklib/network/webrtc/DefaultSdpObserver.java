package net.anandu.blocklib.network.webrtc;

import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;

class DefaultSdpObserver implements SdpObserver {
    @Override
    public void onCreateSuccess(SessionDescription sessionDescription) {
    }

    @Override
    public void onSetSuccess() {
        System.out.println("Set success");

    }

    @Override
    public void onCreateFailure(String s) {
        System.out.println("Creating failed" + s);
    }

    @Override
    public void onSetFailure(String s) {
        System.out.println("Set failed" + s);

    }
}
