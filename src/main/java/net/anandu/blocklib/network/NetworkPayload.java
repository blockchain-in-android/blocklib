package net.anandu.blocklib.network;

public class NetworkPayload {
    private String type;
    private String message;
    private String toId;
    private String peerId;
    private String networkId;

    public NetworkPayload() {
    }

    public NetworkPayload(String type, String message, String peerId, String networkId) {
        this.type = type;
        this.message = message;
        this.peerId = peerId;
        this.networkId = networkId;
    }

    public NetworkPayload(String type, String message, String toId, String peerId, String networkId) {
        this.type = type;
        this.message = message;
        this.toId = toId;
        this.peerId = peerId;
        this.networkId = networkId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPeerId() {
        return peerId;
    }

    public void setPeerId(String peerId) {
        this.peerId = peerId;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }
}
