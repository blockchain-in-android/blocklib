package net.anandu.blocklib.consensus.proof_of_authority;

public enum POANodeType {
    AUTHORITY_NODE, NODE
}
