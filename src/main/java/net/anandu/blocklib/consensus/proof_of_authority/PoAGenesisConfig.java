package net.anandu.blocklib.consensus.proof_of_authority;

import android.content.Context;

import com.google.gson.Gson;

import net.anandu.blocklib.blockchain.Encryption;
import net.anandu.blocklib.consensus.GenesisConfig;
import net.anandu.blocklib.database.AuthorityNode;
import net.anandu.blocklib.database.Block;
import net.anandu.blocklib.database.BlockchainDatabase;
import net.anandu.blocklib.util.StringUtil;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PoAGenesisConfig extends GenesisConfig {
    private final List<String> validators;

    public PoAGenesisConfig(List<String> validators, String extraData, Integer maxTransactionsCount) {
        this.validators = validators;
        this.extraData = extraData;
        this.maxTransactionsCount = maxTransactionsCount;
    }

    public void init(Context c) {
        BlockchainDatabase appdb = BlockchainDatabase.getDatabase(c);
        //add validators to the validatorsTable
        for (String validator : validators) {
            BlockchainDatabase.databaseWriteExecutor.execute(() -> {
                appdb.authorityNodeDao().insert(new AuthorityNode(validator));
            });
        }
    }

    @Override
    public Block createBlock(KeyPair keys) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        Gson gson = new Gson();
        Map<String, String> data = new HashMap<>();
        data.put("Validators", gson.toJson(this.validators));
        data.put("Extra Data", this.extraData);
        data.put("Max Transactions Count", Integer.toString(this.maxTransactionsCount));

        String validator = Encryption.getAddress(keys);
        Encryption encryption = new Encryption(keys);
        String concatenatedString = validator + data;
        byte[] byteArray = concatenatedString.getBytes(StandardCharsets.UTF_8);
        String signature = encryption.sign(byteArray);

        return new Block("", validator, signature, "", data);
    }

    public String calculateHash() {
        return StringUtil.applySha256(this.validators + this.extraData);
    }

    public List<String> getValidators() {
        return validators;
    }

    public String getextraData() {
        return extraData;
    }
}
