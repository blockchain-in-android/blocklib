package net.anandu.blocklib.consensus.proof_of_authority;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.anandu.blocklib.blockchain.Encryption;
import net.anandu.blocklib.consensus.ConsensusInterface;
import net.anandu.blocklib.consensus.MessagePayload;
import net.anandu.blocklib.database.AuthorityNode;
import net.anandu.blocklib.database.Block;
import net.anandu.blocklib.database.BlockWithTransactions;
import net.anandu.blocklib.database.BlockchainDatabase;
import net.anandu.blocklib.database.Transaction;
import net.anandu.blocklib.dto.RequestBlocks;
import net.anandu.blocklib.network.NetworkInterface;
import net.anandu.blocklib.network.NetworkPayload;
import net.anandu.blocklib.network.testnet.TestNetInstance;

import java.lang.reflect.Type;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public abstract class ProofOfAuthorityConsensus extends ConsensusInterface {
    NetworkInterface networkInterface;
    Context context;
    String address;
    POANodeType nodeType;

    public ProofOfAuthorityConsensus(Context c, KeyPair key) throws NoSuchAlgorithmException {
        context = c;
        database = BlockchainDatabase.getDatabase(c);
        networkInterface = TestNetInstance.getInstance();
        address = Encryption.getAddress(key);
        setNodeType(address);
        TestNetInstance.addListener(this::onMessageReceived);
    }

    public void syncChain() {
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        requestCurrentStatus();
    }

    void setNodeType(String address) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            Boolean exists = database.authorityNodeDao().checkIfAuthorityExists(address);
            if (exists)
                nodeType = POANodeType.AUTHORITY_NODE;
            else
                nodeType = POANodeType.NODE;
        });
    }

    void requestCurrentStatus() {
        JsonObject json = new JsonObject();
        json.addProperty("code", "GET_STATUS");
        json.addProperty("data", (String) null);
        String message = new Gson().toJson(json);
        this.networkInterface.broadcast(message);
    }

    void sendCurrentStatus(String toId) {
        //TODO: Only Current leader authority node need to send the block
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            BlockWithTransactions lastBlock = database.blockDao().getLastBlock();
            if (lastBlock != null) {
                String data = new Gson().toJson(lastBlock, BlockWithTransactions.class);
                String message = new Gson().toJson(new MessagePayload("CURRENT_STATUS", data), MessagePayload.class);
                networkInterface.message(toId, "peer_message", message);
            }
        });
    }

    void validateCurrentStatus(String peerId, Block block) {
        //TODO: Check if the current leader auth node sent the block(??)(may be not needed)
        //TODO: Validate the block
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            BlockWithTransactions lastBlock1 = database.blockDao().getLastBlock();
            Block lastBlock = null;
            if(lastBlock1 != null)
                lastBlock = lastBlock1.block;
//            System.out.println("LAST BLOCK STORED IN THE DB " + lastBlock.getData() + " HAS HASH: " + lastBlock.getCurrentHash());
//            System.out.println("BLOCK DATA WE GOT FROM THE NETWORK " + block.getData() + " HAS HASH: " + block.getCurrentHash());
            if (lastBlock != null && lastBlock.getBlockNum().equals(block.getBlockNum())) {
                if (!lastBlock.getCurrentHash().equals(block.getCurrentHash())
                        || !lastBlock.getMerkleRoot().equals(block.getMerkleRoot())) {
                    // TODO: Verify which one is valid and insert that
                }
            } else {
                Integer start = lastBlock == null ? 1 : lastBlock.getBlockNum();
                Integer end = block.getBlockNum();
                if (start <= end)
                    requestBlocks(peerId, start, end);

            }
        });
    }

    void requestBlocks(String toId, Integer start, Integer end) {
        RequestBlocks req = new RequestBlocks(start, end);
        String data = new Gson().toJson(req, RequestBlocks.class);
        String message = new Gson().toJson(new MessagePayload("GET_BLOCKS", data), MessagePayload.class);
        networkInterface.message(toId, "peer_message", message);
    }

    void sendBlocks(String toId, RequestBlocks req) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            Block lastBlock = database.blockDao().getLastBlock().block;
            if (lastBlock.getBlockNum() >= req.getEnd()) {
                List<BlockWithTransactions> blocks = database.blockDao().getBlocksInRange(req.getStart(), req.getEnd());
                Type type = new TypeToken<List<BlockWithTransactions>>() {
                }.getType();
                String data = new Gson().toJson(blocks, type);
                String message = new Gson().toJson(new MessagePayload("REQUESTED_BLOCKS", data), MessagePayload.class);
                networkInterface.message(toId, "peer_message", message);
            }
        });
    }

    public void sendNewTransaction(Transaction transaction) {
        String data = new Gson().toJson(transaction, Transaction.class);
        String message = new Gson().toJson(new MessagePayload("NEW_TRANSACTION", data), MessagePayload.class);
        networkInterface.broadcast(message);
    }

    public void sendNewBlock(BlockWithTransactions block) {
        String data = new Gson().toJson(block, BlockWithTransactions.class);
        String message = new Gson().toJson(new MessagePayload("NEW_BLOCK", data), MessagePayload.class);
        networkInterface.broadcast(message);
    }

    void onMessageReceived(NetworkPayload packet) {
        MessagePayload message = new Gson().fromJson(packet.getMessage(), MessagePayload.class);
        BlockWithTransactions block;
        switch (message.getCode()) {
            case "GET_STATUS":
                sendCurrentStatus(packet.getPeerId());
                break;
            case "CURRENT_STATUS":
                block = new Gson().fromJson(message.getData(), BlockWithTransactions.class);
                validateCurrentStatus(packet.getPeerId(), block.block);
                break;
            case "GET_BLOCKS":
                RequestBlocks req = new Gson().fromJson(message.getData(), RequestBlocks.class);
                sendBlocks(packet.getPeerId(), req);
                break;
            case "REQUESTED_BLOCKS":
                Type type = new TypeToken<List<BlockWithTransactions>>() {
                }.getType();
                List<BlockWithTransactions> blocks = new Gson().fromJson(message.getData(), type);
                addBlocks(blocks);
                break;
            case "NEW_TRANSACTION":
                Transaction transaction = new Gson().fromJson(message.getData(), Transaction.class);
                addTransaction(transaction);
                break;
            case "NEW_BLOCK":
                block = new Gson().fromJson(message.getData(), BlockWithTransactions.class);
                // Override the function
                addBlock(block);
                break;
        }
    }
}
