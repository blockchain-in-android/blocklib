package net.anandu.blocklib.consensus;

import android.content.Context;

import net.anandu.blocklib.database.Block;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

public abstract class GenesisConfig {
    protected Integer maxTransactionsCount;
    protected String extraData;

    protected abstract String calculateHash();

    abstract public void init(Context c);

    abstract public Block createBlock(KeyPair key) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException;

    public Integer getMaxTransactionsCount() {
        return this.maxTransactionsCount;
    }
}
