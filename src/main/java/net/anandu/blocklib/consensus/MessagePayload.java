package net.anandu.blocklib.consensus;

public class MessagePayload {
    private final String code;
    private final String data;

    public MessagePayload(String code, String data) {
        this.code = code;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public String getData() {
        return data;
    }
}
