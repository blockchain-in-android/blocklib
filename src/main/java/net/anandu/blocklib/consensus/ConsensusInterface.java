package net.anandu.blocklib.consensus;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.blockchain.Encryption;
import net.anandu.blocklib.database.AuthorityNode;
import net.anandu.blocklib.database.AuthorityNodeDao;
import net.anandu.blocklib.database.Block;
import net.anandu.blocklib.database.BlockWithTransactions;
import net.anandu.blocklib.database.BlockchainDatabase;
import net.anandu.blocklib.database.Transaction;
import net.anandu.blocklib.util.StringUtil;

import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class ConsensusInterface {
    protected BlockchainDatabase database;

    public abstract void syncChain();

    public abstract void sendNewTransaction(Transaction transaction);

    public abstract void sendNewBlock(BlockWithTransactions block);

    public abstract void onBlockAdded(Integer blockNum);

    public abstract void onTransactionAdded(Transaction transaction);

    public void addBlock(Block block, List<Transaction> transactionsList) {
        // TODO: Check if authority node created that block
        //TODO: check if the block came from authority and check if the block's valid
        //TODO: check the validity of the signature
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            Boolean blockExists = database.blockDao().checkIfBlockExists(block.getCurrentHash());
            if (blockExists) {
                System.out.println("Block already exists in the blockchain");
                //throw new RuntimeException("Block already exists in the blockchain");
            }
            for (Transaction t : transactionsList) {
                Boolean transactionExists = database.transactionDao().checkTransactionExists(t.getHash());
                if (transactionExists) {
                    //throw new RuntimeException("Transaction with transaction hash " + t.getHash() + " already exists in one of the blocks in blockchain");
                    System.out.println("Transaction with transaction hash " + t.getHash() + " already exists in one of the blocks in blockchain");
                }
            }
            Integer blockNum = database.blockDao().insertBlock(block, transactionsList);
            this.onBlockAdded(blockNum);
        });
    }

    public void addBlock(BlockWithTransactions block) {
        this.addBlock(block.block, block.transactions);
    }

    public void addBlocks(List<BlockWithTransactions> blocks) {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            List<AuthorityNode> authNodes = new ArrayList<AuthorityNode>();
            for (BlockWithTransactions b : blocks) {
                Boolean blockExists = database.blockDao().checkIfBlockExists(b.block.getCurrentHash());
                if (blockExists) {
                    System.out.println("Block already exists in the blockchain");
                    //throw new RuntimeException("Block already exists in the blockchain");
                }
                //checking for auth nodes and adding to auth nodes table in db
                Map<String, String> data = b.block.getData();
                Boolean isValidator = database.authorityNodeDao().checkIfAuthorityExists(b.block.getValidator());
                if(data != null && isValidator || b.block.getBlockNum() == 1) {
                    String validators = data.get("Validators");
                    if ( validators != null && validators.length()>2) {
                        String[] validatorArray = validators.substring(1, validators.length()-1).split(",");
                        for(String v : validatorArray) {
                            System.out.println("'"+v.substring(1,v.length()-1)+ "'");
                            database.authorityNodeDao().insert(new AuthorityNode(v.substring(1,v.length()-1)));
                        }
                    }
                    for (Transaction t : b.transactions) {
                        Boolean transactionExists = database.transactionDao().checkTransactionExists(t.getHash());
                        if (transactionExists) {
                            //throw new RuntimeException("Transaction with transaction hash " + t.getHash() + " already exists in one of the blocks in blockchain");
                            System.out.println("Transaction with transaction hash " + t.getHash() + " already exists in one of the blocks in blockchain");
                        }
                    }
                    database.blockDao().insertBlock(blocks);
                }
            }
            // TODO: Call onBlockAdded, currenlty it takes blockNum, which is not applicable here
            //TODO: write a add auth node and call that fun for every auth.
        });
    }

    public void createTransaction(KeyPair keys, String sender, String receiver, Long amount, Map<String, String> data, BlockLib.TransactionValidator transactionValidator) {
        try {
            Encryption encryption = new Encryption();
            String concatenatedString;
            if (amount != null)
                concatenatedString = sender + receiver + amount + data;
            else
                concatenatedString = sender + receiver + data;
            byte[] byteArray = concatenatedString.getBytes(StandardCharsets.UTF_8);
            String signature = encryption.sign(byteArray);
            Transaction transaction = new Transaction(signature, encryption.getPubKeyString(), sender, receiver, amount, data);
            if (transactionValidator != null) {
                if (!transactionValidator.validate(transaction)) {
                    // todo: refactor this to own error class
                    //throw new RuntimeException("Invalid transaction");
                    System.out.println("Invalid transaction");
                }
            }
            this.addTransaction(transaction);
            this.sendNewTransaction(transaction);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void addTransaction(Transaction transaction) {
        //TODO: check the validity of the signature in the transaction, if valid, then add to the chain
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            database.transactionDao().insert(transaction);
            // TODO: uncomment when we fix network from TestNet
//            this.sendNewTransaction(transaction);
            this.onTransactionAdded(transaction);
        });
    }

    //TODO: Calculate a merkle root and compare too
    //TODO: callback interface to be called on success
    public void isValidChain() {
        BlockchainDatabase.databaseWriteExecutor.execute(() -> {
            long blocksCount = database.blockDao().getBlocksCount();
            String prevBlockHash = "";
            for (int i = 1; i <= blocksCount; ++i) {
                Block block = database.blockDao().getBlockHeader(i);
                String calculatedHash = block.calculateHash();
                if (!calculatedHash.equals(block.getCurrentHash())) {
                    System.out.println("Hashes mismatch found in block number " + block.getBlockNum());
                    //throw new RuntimeException("Hashes mismatch found in block number " + block.getBlockNum());
                }
                if (!prevBlockHash.equals(block.getPrevHash())) {
                    int prevBlockNum = block.getBlockNum() - 1;
                    System.out.println("Blockchain seems to be not connected at block " + prevBlockNum + " and " + block.getBlockNum());
                    //throw new RuntimeException("Blockchain seems to be not connected at block " + prevBlockNum + " and " + block.getBlockNum());
                }
            }
        });
    }
}