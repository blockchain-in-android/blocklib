package net.anandu.blocklib;

import android.content.Context;

import com.google.gson.Gson;

import net.anandu.blocklib.blockchain.BlockChain;
import net.anandu.blocklib.consensus.ConsensusInterface;
import net.anandu.blocklib.consensus.GenesisConfig;
import net.anandu.blocklib.consensus.proof_of_authority.PoAGenesisConfig;
import net.anandu.blocklib.consensus.proof_of_authority.ProofOfAuthorityConsensus;
import net.anandu.blocklib.consensus.proof_of_believability.PoBGenesisConfig;
import net.anandu.blocklib.consensus.proof_of_believability.ProofOfBelievabilityConsensus;
import net.anandu.blocklib.database.Transaction;
import net.anandu.blocklib.network.NetworkInterface;
import net.anandu.blocklib.network.testnet.TestNetInstance;
import net.anandu.blocklib.util.AuthorityCallback;
import net.anandu.blocklib.util.BlockCallback;
import net.anandu.blocklib.util.BlockListCallback;
import net.anandu.blocklib.util.CallbackInterface;
import net.anandu.blocklib.util.TransactionListCallback;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.List;
import java.util.Map;

abstract public class BlockLib {
    NetworkInterface network;
    Context c;
    BlockChain blockChain;
    KeyPair keyPair;
    ConsensusInterface consensusMethod;
    CallbackInterface transactionValidator;
    private String peerId;
    private String networkId;

    public void init(
            Context c,
            String peerId,
            KeyPair keyPair,
            GenesisConfig genesisConfig,
            TransactionValidator transactionValidator,
            Boolean onlySync
    ) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        this.peerId = peerId;
        this.c = c;
        this.keyPair = keyPair;
        TestNetInstance.configure("SupplyChain", peerId);

        // this can be improved later
        this.blockChain = new BlockChain(c, transactionValidator) {
            @Override
            public void onConnected() {
                BlockLib.this.onConnected();
            }

            @Override
            public void onTransactionAdded(Transaction t) {
                BlockLib.this.onTransactionAdded(t);
            }

            @Override
            public void onSyncComplete() {
                BlockLib.this.onSyncComplete();
            }

            @Override
            public void onBlockAdded(Integer blockNum) {
                BlockLib.this.onBlockAdded(blockNum);
            }
        };

        // todo: Call Consensus method
        if (genesisConfig instanceof PoAGenesisConfig) {
            System.out.println("THis is proof of authority");
            this.consensusMethod = new ProofOfAuthorityConsensus(c, keyPair) {
                // TODO: add OnConnected and OnSyncComplete functions
                @Override
                public void onTransactionAdded(Transaction t) {
                    BlockLib.this.onTransactionAdded(t);
                }

                @Override
                public void onBlockAdded(Integer blockNum) {
                    BlockLib.this.onBlockAdded(blockNum);
                }

            };
            blockChain.setConsensusMethod(this.consensusMethod);
        } else if (genesisConfig instanceof PoBGenesisConfig) {
            System.out.println("THis is proof of believability");
            this.consensusMethod = new ProofOfBelievabilityConsensus(c, keyPair) {
                // TODO: add OnConnected and OnSyncComplete functions
                @Override
                public void onTransactionAdded(Transaction t) {
                    BlockLib.this.onTransactionAdded(t);
                }

                @Override
                public void onBlockAdded(Integer blockNum) {
                    BlockLib.this.onBlockAdded(blockNum);
                }

            };

            blockChain.setConsensusMethod(this.consensusMethod);
        }
        // maybe a blockchain.start method? [ if needed, ]
        // checkNodeType ->
        if(onlySync)
            this.blockChain.init(keyPair, c);
        else
            this.blockChain.init(genesisConfig, keyPair, c);
    }


    // TODO: Find some other way to expose consensus only functions to end user
    public ConsensusInterface getConsensusMethod() {
        return this.consensusMethod;
    }

    public void createBlock(Map<String, String> data) {
        this.blockChain.createBlock(data, keyPair, c);
    }

    public void createTransaction(String sender, String receiver, Long amount, Map<String, String> data) {
        this.blockChain.createTransaction(keyPair, sender, receiver, amount, data);
    }

    public void getBlock(int num, BlockCallback cb) {this.blockChain.getBlock(num, cb);}
    public void getLastBlock(BlockCallback cb) {
        this.blockChain.getLastBlock(cb);
    }

    public void getBlocks(int start, int end, BlockListCallback cb) {
        this.blockChain.getBlocks(start, end, cb);
    }

    public void checkIfAuthorityExists(String address, AuthorityCallback cb) {
        this.blockChain.checkIfAuthorityExists(address, cb);
    }

    public void searchInBlockData(String query, BlockListCallback cb) {
        // TODO: Paginate this, maybe too much data
        this.blockChain.searchInBlockData(query, cb);
    }

    public void getBlockWithValidatorAndData(String validator, String query, BlockListCallback cb) {
        this.blockChain.getBlockWithValidatorAndData(validator, query, cb);
    }

    public void getBlockWithValidatorAndData(String validator, Map<String, String> query, BlockListCallback cb) {
        Gson gson = new Gson();
        String data = gson.toJson(query);
        this.blockChain.getBlockWithValidatorAndData(validator, data, cb);
    }
    public void getTransactions(Integer numOfTransactions, TransactionListCallback cb) {
        this.blockChain.getTransactions(numOfTransactions, cb);
    }

    // TODO: Remove this later, incomplete
    public List<Transaction> searchTransaction(CallbackInterface cb) {
        return this.blockChain.searchTransaction(cb);
    }

    public void searchInTransactionData(String query, TransactionListCallback cb) {
        this.blockChain.searchInTransactionData(query, cb);
    }

    public void getTransactionWithSenderAndData(String sender, String query, TransactionListCallback cb) {
        this.blockChain.getTransactionWithSenderAndData(sender, query, cb);
    }

    public void getTransactionWithReceiverAndData(String receiver, Map<String, String> query, TransactionListCallback cb) {
        Gson gson = new Gson();
        String data = gson.toJson(query);
        this.blockChain.getTransactionWithReceiverAndData(receiver, data, cb);
    }

    public void getTransactionsCreatedAfterTime(Long time, TransactionListCallback cb) {
        this.blockChain.getTransactionsCreatedAfterTime(time, cb);
    }

    abstract public void onTransactionAdded(Transaction t);

    abstract public void onConnected();

    abstract public void onSyncComplete();

    abstract public void onBlockAdded(Integer blockNum);

    public KeyPair getKeyPair() {
        return keyPair;
    }

    public String getPeerId() {
        return peerId;
    }

    public interface TransactionValidator {
        boolean validate(Transaction t);
    }
}
